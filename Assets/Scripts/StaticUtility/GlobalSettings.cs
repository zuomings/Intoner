using UnityEngine;
using System.Collections;
using Exercise;

namespace Globals
{
    // TODO: use Path.Combine for platform dependent path strings.
    // All global timing data
    public static class GlobalSettings
    {
        public readonly static float tempo = 85f;
        public readonly static float internoteTiming = 60f / tempo;
        public readonly static float endSequenceTiming = 0.6f;
        public readonly static int lowestNote = 0;
        public readonly static int highestNote = 87;
        public readonly static float correctJingleDuration = 0.5f;
        public readonly static float incorrectJingleDuration = 0.5f;
        public readonly static int maxQuestionsCount = 200;

        public readonly static int iterationsTillKeySwitch = 5;
        public readonly static CorrectPolicy correctPolicy = CorrectPolicy.NEXT;
        public readonly static IncorrectPolicy incorrectPolicy = IncorrectPolicy.BOO_REPLAY;
        public readonly static bool mOtherOctavesOK = false;

        private readonly static string correctJinglePath = "Sounds/correct2";
        private readonly static string incorrrectJinglePath = "Sounds/incorrect2";
        public static AudioClip correctSound;
        public static AudioClip incorrectSound;

        public static readonly System.DateTime startTime = System.DateTime.Now;

        public readonly static string defaultInstrumentsDirectory = "Sounds/Instruments/";
        public readonly static string defaultInstrument = "Harpsichord";
        public readonly static string defaultSavedExercisesAndStatsPath = "Saved/";

        public readonly static float buttonGulf = 0.1f;

        public readonly static bool applyResolveLeadingNotesBeforeFinalChordHeuristic = true;
        //public static int buttonColumns = 2;
        //public static int buttonRows = 4;

        public static bool initialized = false;
        public static void Initialize() {
            if (!initialized)
            {
                initialized = true;
                correctSound = Resources.Load<AudioClip>(correctJinglePath);
                incorrectSound = Resources.Load<AudioClip>(incorrrectJinglePath);
                return;
            }
        }

        public enum PracticeStrategy {
            MULTIPLE_GUESSES,
            COMPARE_ANSWERS
        }

        public readonly static PracticeStrategy practiceMode = PracticeStrategy.COMPARE_ANSWERS;
        public readonly static bool CanRepeatSameNote = false;
        public readonly static bool PickCloserNoteToPreview = true;
    }
}