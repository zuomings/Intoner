﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Exercise;
using Globals;
using System.Xml.Serialization;

namespace SaveLoad
{
    public static class SaveLoadManager
    {
        private static Dictionary<string, ExerciseAndStats> savedExercises = new Dictionary<string, ExerciseAndStats>();
        private static KeyValuePair<string, ExerciseAndStats> currentExercise = new KeyValuePair<string, ExerciseAndStats>(null, null);
        private static string persistentPath = Application.persistentDataPath + GlobalSettings.defaultSavedExercisesAndStatsPath;
        private static string exercisePath;

        public static void Reinitialize(string filepath = null)
        {
            savedExercises.Clear();
            exercisePath = filepath == null ? persistentPath : filepath;
            Debug.Log("File path is at " + exercisePath);

            if (!Directory.Exists(exercisePath))
            {
                Directory.CreateDirectory(exercisePath);
            }

            var saveDirectoryInfo = new DirectoryInfo(exercisePath);

            var fileInfos = saveDirectoryInfo.GetFiles();

            foreach (var fileInfo in fileInfos)
            {
                if (fileInfo.Name[0] == '.') { continue; } // ignore hidden files.
                Debug.Log("Found savefile " + fileInfo.Name);
                /*
                FileStream file = File.Open(Application.persistentDataPath +
                                            GlobalSettings.defaultSavedExercisesAndStatsPath +
                                            fileInfo.Name, FileMode.Open);*/
                /*
                StreamReader reader = new StreamReader(persistentPath + fileInfo.Name);
                ExerciseAndStats exerciseAndStats = Tiny.Json.Decode<ExerciseAndStats>(reader.ReadToEnd());
                savedExercises.Add(fileInfo.Name, exerciseAndStats);
                reader.Close();
                */

                var stream = new FileStream(exercisePath + fileInfo.Name, FileMode.Open);

                try
                {
                    var serializer = new XmlSerializer(typeof(ExerciseAndStats));
                    var exerciseAndStats = serializer.Deserialize(stream) as ExerciseAndStats;
                    savedExercises.Add(fileInfo.Name, exerciseAndStats);
                }
                catch 
                {
                    continue;
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        public static void SaveExercise(ExerciseAndStats exerciseAndStats, string filename) {
            /*
            StreamWriter writer = new StreamWriter(persistentPath + filename);
            string exerciseJson = Tiny.Json.Encode(exerciseAndStats);
            writer.Write(exerciseJson);
            writer.Close();
            */
            var serializer = new XmlSerializer(typeof(ExerciseAndStats));
            var stream = new FileStream(exercisePath + filename, FileMode.Create);
            serializer.Serialize(stream, exerciseAndStats);
            stream.Close();

            //Debug.Log("Saving exercise " + exerciseJson);
        }

        public static Dictionary<string, ExerciseAndStats> GetSavedExercises()
        {
            return savedExercises;
        }

        public static void LoadExerciseWithoutStarting(string exerciseFile)
        {
            currentExercise = new KeyValuePair<string, ExerciseAndStats>(exerciseFile, savedExercises[exerciseFile]);
        }

        public static void SaveCurrentExercise()
        {
            SaveExercise(currentExercise.Value, currentExercise.Key);

            /*
            if (currentExercise.Key != null)
            {
                savedGames.Add(Game.current);
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
                bf.Serialize(file, SaveLoad.savedGames);
                file.Close();
            }
            */
        }

        public static KeyValuePair<string, ExerciseAndStats> GetCurrentExercisePair()
        {
            return currentExercise;
        }
    }
}