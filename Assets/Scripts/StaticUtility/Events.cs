using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Globals
{
    public class EventSource
    {
        private List<EventListener> mListeners;
        public EventSource()
        {
            mListeners = new List<EventListener>();
        }

        public void Subscribe(EventListener listener)
        {
            if (mListeners.Contains(listener))
            {
                Logger.LogError("Listener already exists.");
            }
            else
            {
                mListeners.Add(listener);
            }
        }

        public void OnUpdate()
        {
            mListeners.ForEach(listener => { listener.OnUpdate(); });
        }

        public void Unsubscribe(EventListener listener)
        {
            if (mListeners.Contains(listener))
            {
                mListeners.Remove(listener);
            }
            else
            {
                Logger.LogError("Listener already removed.");
            }
        }
    }

    public class EventListener
    {
        public delegate void OnUpdateCall();
        public OnUpdateCall mCall;

        public EventListener(OnUpdateCall call)
        {
            mCall = call;
        }

        public void OnUpdate()
        {
            mCall();
        }
    }
}
