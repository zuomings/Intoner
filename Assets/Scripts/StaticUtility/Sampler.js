var sample : AudioClip;
var numberOfNotes : int;

private var pitches : GameObject[];
private var pitchInput : String[];
private var shiftRange : int;

function Start ()
{
	pitches = new GameObject[numberOfNotes];
	pitchInput = new String[numberOfNotes];
	var twelfthRootOfTwo : float = Mathf.Pow(2, 1.0/12);
	for (var i : int = 0; i < numberOfNotes; i++) 
	{
		//one more note on the high end for even numberOfNotes,
		//because removing samples is easier than inventing them
		shiftRange = Mathf.CeilToInt(numberOfNotes *.5 - .1);	//- .1 because Mathf.CeilToInt is broken right now
		var shift : int = i - shiftRange + 1;	
		var semitoneShift : String = shift.ToString();
		
		//each of these GameObjects plays one pitched AudioClip
		pitches[i] = new GameObject(semitoneShift, AudioSource);
		pitches[i].GetComponent.<AudioSource>().playOnAwake = false;
		pitches[i].GetComponent.<AudioSource>().clip = sample;
		pitches[i].GetComponent.<AudioSource>().pitch = Mathf.Pow(twelfthRootOfTwo, shift);
		
		//create strings for GetButtonDown
		pitchInput[i] = semitoneShift;
	}
}

function Update () 
{
	for (var semitone : String in pitchInput) {
		if (Input.GetButtonDown(semitone))
		{
			var semitoneNote : int = int.Parse(semitone);
			var note : int = semitoneNote + shiftRange -1;
			pitches[note].GetComponent.<AudioSource>().PlayOneShot(pitches[note].GetComponent.<AudioSource>().clip);
			Debug.Log(semitoneNote);
		}
	}
}