﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using DG.Tweening;

namespace Globals
{
    public class Initializer
    {
        private static bool mInitialized = false;
        public static void Inialize() {
            if (!mInitialized)
            {
                GlobalSettings.Initialize();
                PitchUtils.Initialize();
                AudioUtils.Initialize();
                SaveLoad.SaveLoadManager.Reinitialize();
                mInitialized = true;
            }
        }
    }

    public class Utils
    {
        public delegate void BasicCallback();

        public static void LogMethodEntry() {
            UnityEngine.Debug.Log(new StackFrame(1).GetMethod().Name);
        }

        public static int RandomInt(int i) {
            return Mathf.FloorToInt(Random.Range(0, i));
        }

        public static T GetRandom<T>(IList<T> list)
        {
            return list[Mathf.FloorToInt(Random.Range(0, list.Count))];
        }

        public static ViewFrame GetViewFrame()
        {
            return GameObject.Find("ViewFrame").GetComponent<ViewFrame>();
        }

        public static _Mono MakeSpritedMono(string name = "SpriteMono")
        {
            //string whiteSquare1x1path = "climb";
            GameObject go = new GameObject(name);
            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            Texture2D tex = Resources.Load<Texture2D>("sprites/white1x1");

            sr.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 1.0f);
            sr.color = Color.black;
            return go.AddComponent<_Mono>();
        }

        public static _Mono MakeSpritedMonoWithCollider(string name = "SpritedMonoWithCollider") {
            _Mono obj = MakeSpritedMono(name);
            BoxCollider box = obj.gameObject.AddComponent<BoxCollider>();
            box.size = new Vector3(1f, 1f, 1f);
            return obj;
        }

        public static _Mono MakeCubedMono(string name = "CubedMono")
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.name = name;
            return cube.AddComponent<_Mono>();
        }

        public static Sequence MakeOrderedSequence(DG.Tweening.TweenCallback cb) {
            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(0.00001f);
            sequence.AppendCallback(cb);
            return sequence;
        }

        public static List<KeyValuePair<T1, T2>> DictToList<T1, T2>(Dictionary<T1, T2> dict) {
            var list = new List<KeyValuePair<T1, T2>>();
            foreach(KeyValuePair<T1, T2> pair in dict)
            {
                list.Add(pair);
            }
            return list;
        }
    }
}