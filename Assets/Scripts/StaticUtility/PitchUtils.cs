﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Globals
{
    [System.Serializable]
    public class Pitch: PlayableConstruct
    {
        public PitchClass mPitchClass;
        public int mOctave;
        public int mPitchIndex;
        public string name {
            get {
                return PitchUtils.pitchFileNames[(int)mPitchClass] + mOctave;
            }
        }

        public Pitch(PitchClass pitchClass, int octave, int pitchIndex)
        {
            mPitchClass = pitchClass;
            mOctave = octave;
            mPitchIndex = pitchIndex;
        }

        public Pitch(PitchClass pitchClass, int octave)
        {
            mPitchClass = pitchClass;
            mOctave = octave;
            mPitchIndex = Globals.PitchUtils.GetPitchIndex(this);
        }

        public Pitch(int pitchIndex)
        {
            mPitchIndex = pitchIndex;
            mPitchClass = Globals.PitchUtils.GetPitchClassFromIndex(pitchIndex);
            mOctave = Globals.PitchUtils.GetPitchOctaveFromIndex(pitchIndex);
        }

        // Ideally this does not have to be used too often.
        public Pitch(string name): this(
            PitchUtils.GetPitchClassFromName(name.Substring(0, name.Length - 1)), int.Parse(name.Substring(name.Length - 2, 1)))
        {
        }

        public int Subtract(Pitch p)
        {
            return PitchUtils.GetPitchIndex(this) - PitchUtils.GetPitchIndex(p);
        }

        // Interface PlayableConstruct
        public NoteChart GetChart()
        {
            NoteChart chart = new NoteChart();
            chart.mNotes.Add(new NoteChart.Note(this, 0.0f));
            return chart;
        }

        public override bool Equals(System.Object obj)
        {
            return obj is Pitch && this == (Pitch)obj;
        }

        public static bool operator ==(Pitch x, Pitch y)
        {
            return x.mPitchClass == y.mPitchClass && 
                    x.mOctave == y.mOctave &&
                    x.mPitchIndex == y.mPitchIndex;
        }

        public static bool operator !=(Pitch x, Pitch y)
        {
            return !(x == y);
        }

        public override int GetHashCode()
        {
            return mPitchClass.GetHashCode() ^ mOctave.GetHashCode();
        }
    }

    [System.Serializable]
    public class NoteSample
    {
        public Pitch mPitch;
        public AudioClip mClip;

        public NoteSample(Pitch pitch, AudioClip clip)
        {
            mPitch = pitch;
            mClip = clip;
        }
    }

    public enum PitchClass
    {
        A = 0,
        AsBb,
        B,
        C,
        CsDb,
        D,
        DsEb,
        E,
        F,
        FsGb,
        G,
        GsAb
    }

    public static class PitchUtils
    {
        public static List<string> pitchNames = new List<string>{ "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };
        public static List<string> pitchFileNames = new List<string> { "A", "As", "B", "C", "Cs", "D", "Ds", "E", "F", "Fs", "G", "Gs" };
        public static int centerAIndex;
        public static Pitch centerA;
        public static List<Pitch> allNotes;
        public static int octaves = 8;
        public static bool initialized = false;

        public static void Initialize()
        {
            // Populate list of all notes
            allNotes = new List<Pitch>();
            for (int i = 0; i < GlobalSettings.highestNote + 1; i++)
            {
                //ref 88 note piano: https://en.wikipedia.org/wiki/Piano_key_frequencies
                allNotes.Add(new Pitch((PitchClass)(i % 12), (i + 10) / 12, i));
            }

            centerAIndex = allNotes.FindIndex(p => ((p.mPitchClass == PitchClass.A) && (p.mOctave == 4)));
            centerA = allNotes[centerAIndex];
            // Populate the list of NoteSamples

            initialized = true;
        }

        public static PitchClass GetPitchClassFromName(string name)
        {
            return (PitchClass)pitchNames.FindIndex(x => x.Equals(name));
        }

        public static PitchClass GetPitchClassShift(PitchClass pitchClass, int shift)
        {
            return GetPitchClassFromName(pitchNames[((int)pitchClass + shift) % 12]);
        }

        public static Pitch GetPitchShift(Pitch pitch, int i)
        {
            return allNotes[pitch.mPitchIndex + i];
        }

        public static string GetNameFromPitchClass(PitchClass pclass)
        {
            return pitchNames[(int)pclass];
        }

        public static int GetPitchIndex(Pitch p)
        {
            return GetPitchIndex(p.mPitchClass, p.mOctave);
        }

        public static int GetPitchIndex(PitchClass pitchClass, int octave)
        {
            return allNotes.FindIndex(p => ((pitchClass == p.mPitchClass) && (p.mOctave == octave)));
        }

        public static PitchClass GetPitchClassFromIndex(int index)
        {
            return (PitchClass)(index % 12);
        }

        public static int GetPitchOctaveFromIndex(int index)
        {
//            Debug.Log(index);
            return allNotes[index].mOctave;
        }
    }

    // Able to generate a playchart
    public interface PlayableConstruct
    {
        NoteChart GetChart();
    }

    // Can be thought of as a midi piano roll: timing + pitch information are included.
    public class NoteChart
    {
        public class Note
        {
            public Pitch mPitch;
            public float mTime;
            public Note(Pitch pitch, float time)
            {
                mPitch = pitch;
                mTime = time;
            }
        }

        public NoteChart()
        {
            mNotes = new List<Note>();
        }

        public List<Note> mNotes { get; set; }

        public float mDuration
        {
            get { return mNotes.Max(note => note.mTime) + GlobalSettings.endSequenceTiming; }
        }
    }

    public static class AudioUtils
    {
        public static List<NoteSample> orderedDefaultSamples; // sort by pitch index
        public static string sampleDir = "Clav"; // For later
        private static float preferRaiseToLowerRatio = 1.5f; // Raising by 12 steps is as preferable as lower by 8 steps.
        public static bool initialized = false;

        public static void Initialize()
        {
            // Populate the list of NoteSamples
            orderedDefaultSamples = PrepareInstrumentSamples(GlobalSettings.defaultInstrument);

            initialized = true;
        }

        public static List<NoteSample> PrepareInstrumentSamples(string instrument)
        {
            AudioClip[] notes = Resources.LoadAll<AudioClip>(
                GlobalSettings.defaultInstrumentsDirectory + instrument + "/");

            List<NoteSample> samples = new List<NoteSample>();

            PitchUtils.allNotes.ForEach((pitch) =>
            {
                for (int i = 0; i < notes.Count(); i++)
                {
                    if (pitch.name == notes[i].name)
                    { 
                        samples.Add(new NoteSample(pitch, notes[i])); 
                    }
                }
            });

            return samples;
        }

        // Given a difference in semitones (positive or negative, outputs the shift necessary to transpose that many tones.
        private static float GetPitchShift(float shift)
        {
            return Mathf.Pow(2f, shift / 12f);
        }

        private static float GetPitchShiftFromCenterA(Pitch p)
        {
            return Mathf.Pow(2f, p.mPitchIndex - PitchUtils.centerAIndex);
        }

        // Play a specific sample at a specific pitch
        private static void PlaySampleAtPitch(NoteSample noteSample, Pitch pitch)
        {
            Utils.LogMethodEntry();
            //Debug.Break();
            GameObject soundPlayer = new GameObject(pitch.name);
            soundPlayer.AddComponent<NoteDebug>().pitch = pitch;
            soundPlayer.AddComponent<SelfDescructScript>();
            AudioSource asc = soundPlayer.AddComponent<AudioSource>();
            asc.clip = noteSample.mClip;
            asc.pitch = PitchShifter.GetPitchShift(pitch.Subtract(noteSample.mPitch));
            asc.Play();
        }

        private static void PlayChord(RealizedChord chord)
        {
            chord.mPitches.ForEach((pitch) => { PlayPitch(pitch); });
        }

        // Play a specific pitch, picking from the default sample
        private static void PlayPitch(Pitch pitch)
        {
            Utils.LogMethodEntry();
            float minDist = float.MaxValue;
            int bestSampleIndex = 0;
            for (int i = 0; i < orderedDefaultSamples.Count; i++)
            {

                int difference = pitch.Subtract(orderedDefaultSamples[i].mPitch);

                // Raising vs. lowering a pitch given different properties.
                float score = difference > 0 ? difference / preferRaiseToLowerRatio : -difference;
                if (score < minDist)
                {
                    bestSampleIndex = i;
                    minDist = score;
                }
            }

            PlaySampleAtPitch(orderedDefaultSamples[bestSampleIndex], pitch);
        }

        // Play a specific pitch, picking from the default sample
        public static void PlayAudioClip(AudioClip clip)
        {
            GameObject soundPlayer = new GameObject();
            soundPlayer.AddComponent<SelfDescructScript>();
            AudioSource asc = soundPlayer.AddComponent<AudioSource>();
            asc.clip = clip;
            asc.Play();
        }

        // Return Sequence of musical events, each note will be a seperate object.
        public static Sequence Play(PlayableConstruct pc)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            NoteChart chart = pc.GetChart();
            float lastNoteTime = 0.0f;
            foreach (var note in chart.mNotes)
            {
                sequence.AppendInterval(note.mTime - lastNoteTime);
                sequence.AppendCallback(() => { PlayPitch(note.mPitch); });
                lastNoteTime = note.mTime;
            }
            sequence.AppendInterval(GlobalSettings.endSequenceTiming);

            // TODO: import teweening library, call invoke.
            // return chart.mDuration;
            // Todo: write sequence logic.
            return sequence;
        }

        public static Sequence Play(AudioClip ac, float duration)
        {
            Sequence sequence = DOTween.Sequence();
            sequence.AppendCallback(() => { PlayAudioClip(ac);});
            sequence.AppendInterval(duration);
            return sequence;
        }
    }
}