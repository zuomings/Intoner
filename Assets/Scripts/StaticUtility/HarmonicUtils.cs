using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Globals
{
    // Public class ModifiedChord - inversions, 7th, harmonics, etc.

    [System.Serializable]
    public class RealizedChord: PlayableConstruct
    {
        public List<Pitch> mPitches;
        public string mDisplayName;

        // Method to generate chord 1: given pitch, then quality. Specified in scaleless contexts.
        public RealizedChord(Pitch pitch, Chord chordQuality)
        {
            mPitches = new List<Pitch>();
            chordQuality.mNotes.ForEach((value) => { 
                mPitches.Add(new Pitch(pitch.mPitchIndex + value)); 
            });
            mDisplayName = pitch.mPitchClass + pitch.mOctave.ToString() + " " + chordQuality.mDisplayName;
        }

        // Method to generate chord 2: given scale, specify degree only. Sepcified in context where scales are relevant.
        // Note: callback might be useful.
        public RealizedChord(Key key, int degree) {
            mPitches = new List<Pitch>();
            // Take advantage of the fact that the chord notes are usually 2 and 2 apart.
            new List<int> { 0, 2, 4 }.ForEach((value) => { 
                mPitches.Add(new Pitch((int)key.mTonic + PitchUtils.centerAIndex + key.mScaleIntervals.mNotes[value])); 
            });
            mDisplayName = PitchUtils.GetNameFromPitchClass(key.mTonic) + " " + HarmonicUtils.degreeNames[degree];
        }

        // Method to generate chord 3: given scale, specify degree and quality. Sepcified in context where scales are relevant.
        // Note: callback might be useful.
        public RealizedChord(Key key, ChordNotation notation)
        {
            mPitches = new List<Pitch>();
            notation.mQuality.mNotes.ForEach((value) => {
                mPitches.Add(new Pitch((int)key.mTonic + PitchUtils.centerAIndex + notation.mDegree + value));
            });
            mDisplayName = HarmonicUtils.degreeNames[notation.mDegree] + " " + notation.mQuality.mDisplayName;
        }

        // Interface PlayableConstruct
        public NoteChart GetChart() {
            NoteChart chart = new NoteChart();
            mPitches.ForEach(pitch => { chart.mNotes.Add(new NoteChart.Note(pitch, 0.0f)); });
            return chart;
        }
    }

    [System.Serializable]
    public class Chord
    {
        public List<int> mNotes;
        public string mDisplayName;
        public static Chord ERROR = new Chord(new List<int> { 0, 1, 2 }, "ERROR");
        public static Chord MAJ = new Chord(new List<int> { 0, 4, 7 }, "MAJ");
        public static Chord MIN = new Chord(new List<int> { 0, 3, 7 }, "MIN");
        public static Chord DIM = new Chord(new List<int> { 0, 3, 6 }, "DIM");
        public static Chord AUG = new Chord(new List<int> { 0, 4, 8 }, "AUG");
        public static Chord MIN7 = new Chord(new List<int> { 0, 3, 7, 10}, "MIN7");
        public static Chord MAJ7 = new Chord(new List<int> { 0, 4, 7, 11 }, "MAJ7");
        public static Chord MIN_MAJ7 = new Chord(new List<int> { 0, 3, 7, 11 }, "MIN-MAJ7");

        // default constructor required for serealization
        public Chord(){}

        public Chord(List<int> notes, string displayName = "")
        {
            mNotes = notes;
            mDisplayName = displayName;
        }
    }

    [System.Serializable]
    public class RealizedChordProgression : PlayableConstruct
    {
        public List<RealizedChord> mChords;
        public string mDisplayName;
        public Key mKey;

        /* Initializing without a key is unsupported for now, since key is required for heuristics to work.
        public RealizedChordProgression(List<RealizedChord> chords, string displayName = "")
        {
            mChords = chords;
            mDisplayName = displayName;
        }
        */

        public RealizedChordProgression(Key key, List<ChordNotation> notations, string displayName ="")
        {
            mChords = new List<RealizedChord>();
            mKey = key;
            notations.ForEach(notation => {
                mChords.Add(
                    new RealizedChord(
                        new Pitch((int)mKey.mTonic + PitchUtils.centerAIndex - 12 + mKey.mScaleIntervals.mNotes[notation.mDegree]), 
                        notation.mQuality)
                );
            });
        }

        // Interface PlayableConstruct
        public NoteChart GetChart()
        {
            NoteChart chart = new NoteChart();

            if (GlobalSettings.applyResolveLeadingNotesBeforeFinalChordHeuristic)
            {
                //TODO: support for modes will require different logics for this heuristic.
                //TODO: add leading tones support for 4-3
                var leadingPitchesAndResolutions = new Dictionary<Pitch, int>();
                if (mChords.Count > 1) 
                {
                    Dictionary<PitchClass, int> leadingToneResolutions = mKey.GetLeadingTonesAndResolutions();
                    mChords[mChords.Count - 2].mPitches.ForEach(pitch =>
                    {
                        if (leadingToneResolutions.ContainsKey(pitch.mPitchClass))
                        {
                            leadingPitchesAndResolutions.Add(pitch, leadingToneResolutions[pitch.mPitchClass]);
                        }
                    });
                }

                foreach(var item in leadingPitchesAndResolutions) 
                {
                    Pitch shiftedPitch = PitchUtils.GetPitchShift(item.Key, item.Value);
                    if (!(mChords[mChords.Count - 1].mPitches.Contains(shiftedPitch)))
                    {
                        mChords[mChords.Count - 1].mPitches.Add(shiftedPitch);
                    }
                }
            }

            for (int i = 0; i < mChords.Count; i++)
            {
                mChords[i].mPitches.ForEach(pitch => {
                    chart.mNotes.Add(new NoteChart.Note(pitch, i * GlobalSettings.internoteTiming));
                });
            }

            return chart;
        }
    }

    [System.Serializable]
    public class Key
    {
        //TODO: rename to key
        public Scale mScaleIntervals;
        public PitchClass mTonic;
        public string mDisplayName;
        public Key(PitchClass tonic, Scale scale, string displaname = "")
        {
            mTonic = tonic;
            mScaleIntervals = scale;
            mDisplayName = PitchUtils.GetNameFromPitchClass(tonic) + " " + scale.mDisplayName;
        }

        public RealizedChordProgression GetCenteralChordProgression()
        {
            return new RealizedChordProgression(this, mScaleIntervals.mCharacteristicProgression);
        }

        public Dictionary<PitchClass, int> GetLeadingTonesAndResolutions()
        {
            var dict = new Dictionary<PitchClass, int>();
            dict.Add(PitchUtils.GetPitchClassShift(mTonic, 1), -1); // Minor 2nd to tonic.
            dict.Add(PitchUtils.GetPitchClassShift(mTonic, 2), -2); // Major 2nd to tonic.
            dict.Add(PitchUtils.GetPitchClassShift(mTonic, 11), 1); // Major 7th to tonic.
            dict.Add(PitchUtils.GetPitchClassShift(mTonic, 5), -1); // Perfect 4th to Major 3rd.
            dict.Add(PitchUtils.GetPitchClassShift(mTonic, 6), 1); // Tritone to Perfect 5th.
            return dict;
        }
    }

    [System.Serializable]
    public class ChordNotation
    {
        public int mDegree;
        public Chord mQuality;

        // default constructor needed for serealization
        public ChordNotation(){}

        public ChordNotation(int degree, Chord quality){
            mQuality = quality;
            mDegree = degree;
        }
    }

    [System.Serializable]
    public class Scale
    {
        public static Scale MAJOR = new Scale(
            new List<int> { 0, 2, 4, 5, 7, 9, 11 }, 
            new List<ChordNotation> {
                new ChordNotation(0, Chord.MAJ),
                new ChordNotation(0, Chord.MAJ7),
                new ChordNotation(3, Chord.MAJ),
                new ChordNotation(4, Chord.MAJ),
                new ChordNotation(0, Chord.MAJ)
            },
            "MAJOR");
        public static Scale NARTUAL_MINOR = new Scale(
            new List<int> { 0, 2, 3, 5, 7, 8, 10 }, 
            new List<ChordNotation> {
                new ChordNotation(0, Chord.MIN),
                new ChordNotation(0, Chord.MIN7),
                new ChordNotation(3, Chord.MIN),
                new ChordNotation(4, Chord.MIN),
                new ChordNotation(0, Chord.MIN)
            },
            "MINOR");
        public static Scale MELODIC_MINOR = new Scale(
            new List<int> { 0, 2, 3, 5, 7, 8, 10 }, 
            new List<ChordNotation> {
                new ChordNotation(0, Chord.MIN),
                new ChordNotation(0, Chord.MIN7),
                new ChordNotation(3, Chord.MIN),
                new ChordNotation(4, Chord.MAJ),
                new ChordNotation(0, Chord.MIN)
            },
            "MELODIC MINOR");
        public static Scale HARMONIC_MINOR = new Scale(
            new List<int> { 0, 2, 3, 5, 7, 8, 11 },
            new List<ChordNotation> {
                new ChordNotation(0, Chord.MIN),
                new ChordNotation(0, Chord.MIN_MAJ7),
                new ChordNotation(3, Chord.MIN),
                new ChordNotation(4, Chord.MAJ),
                new ChordNotation(0, Chord.MIN)
            },
            "HARMONIC MINOR");
        
        public List<int> mNotes;
        public string mDisplayName;
        public List<ChordNotation> mCharacteristicProgression;

        // Default constructor needed for seralization
        public Scale(){}

        public Scale(List<int> notes, List<ChordNotation> characteristicProgression, string displaname = "")
        {
            mNotes = notes;
            mDisplayName = mDisplayName;
            mCharacteristicProgression = characteristicProgression;
        }
    }

    public static class HarmonicUtils
    {
        public static bool initialized = false;
        public static Key defaultScale;
        public static string[] degreeNames = { "I", "II", "III", "IV", "V", "VI", "VII" };
        public static RealizedChordProgression defaultProgression;
        private static List<int> defaultProgressionDegrees = new List<int>{ 0, 3, 4, 0 };
        // Prerequisite: PitchUtils initialization
        public static void Initialize() {
            defaultScale = new Key(PitchClass.A, Scale.MAJOR);
            List<RealizedChord> chords = new List<RealizedChord>();
            defaultProgressionDegrees.ForEach(deg => {chords.Add(new RealizedChord(defaultScale, 0));});
            //TODO: figure out if I ever need a default chord progression
            //defaultProgression = new RealizedChordProgression(chords);
            initialized = true;
        }
    }
}