using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Globals;
using DG.Tweening;
using System.Xml.Serialization;

namespace Exercise
{
    public enum CorrectPolicy
    {
        NEXT,
        JINGLE
    }

    public enum IncorrectPolicy
    {
        BOO,
        BOO_REPLAY
    }

    [XmlRoot("ExerciseAndStats")]
    [System.Serializable]
    public class ExerciseAndStats
    {
        public static ExerciseAndStats Default = new ExerciseAndStats(DegreeExerciseProperties.Default, new ExerciseStats());
        public ExerciseProperties mExerciseProperties;
        public ExerciseStats mExerciseStats;
        public ExerciseAndStats()
        {
            
        }

        public ExerciseAndStats(DegreeExerciseProperties properties, ExerciseStats stats)
        {
            mExerciseStats = stats;
            mExerciseProperties = properties;
        }
    }

    [System.Serializable]
    [XmlInclude(typeof(DegreeExerciseProperties))]
    public abstract class ExerciseProperties
    {
        public static Dictionary<string, System.Type> propertyToControllerMap =
            new Dictionary<string, System.Type>()
        {
            {"DegreeExercise", typeof(DegreeExerciseController)}
        };
        public static Dictionary<System.Type, string> controllerToPropertyMap =
            new Dictionary<System.Type, string>()
        {
            {typeof(DegreeExerciseController), "DegreeExercise"}
        };
        public string mControllerClass;
        public int mInterationsTillKeySwitch;
        public CorrectPolicy correctPolicy;
        public IncorrectPolicy incorrectPolicy;
        public bool mOtherOctavesOk;
        public HashSet<PitchClass> mTonics;
        public List<int> mAnswerOctaves;
    }

    [System.Serializable]
    public class DegreeExerciseProperties : ExerciseProperties
    {
        public List<int> mDegrees;
        public HashSet<Scale> mScaleQuality;
        public static DegreeExerciseProperties Default = new DegreeExerciseProperties
        {
            mControllerClass = ExerciseProperties.controllerToPropertyMap[typeof(DegreeExerciseController)],
            mInterationsTillKeySwitch = GlobalSettings.iterationsTillKeySwitch,
            correctPolicy = CorrectPolicy.JINGLE,
            incorrectPolicy = IncorrectPolicy.BOO_REPLAY,
            mOtherOctavesOk = false,
            mTonics = new HashSet<PitchClass> { 
                PitchClass.A, 
                PitchClass.AsBb, 
                PitchClass.B, 
                PitchClass.C, 
                PitchClass.CsDb, 
                PitchClass.D, 
                PitchClass.DsEb, 
                PitchClass.E,
                PitchClass.F,
                PitchClass.FsGb, 
                PitchClass.G,
                PitchClass.GsAb },
            mScaleQuality = new HashSet<Scale> { Scale.MAJOR },
            //mDegrees = new List<int> { 1, 2, 3, 4, 5, 6 },
            mDegrees = new List<int> { 4, 6 },
            mAnswerOctaves = new List<int> { -1, 0 }
        };
        // Hmmm... initialization order may wreak harcov on this, unless all fields have default values.
    }
    /* Possible future exerises.
    public class ChordProgExerciseProperties
    {
        public HashSet<Globals.Key> mScales;
        public HashSet<List<Globals.ChordNotation>> mChords;
    }

    public class ChordExerciseProperties
    {
        public HashSet<Globals.Key> mScales;
        public HashSet<Globals.Chord> mChords;
    }

    public class InvervalHarmonicExerciseProperties
    {
        public HashSet<PitchClass> mAllowableIntervals;
    }
    */

    public class ExerciseStats
    {
        public ExerciseStats() {}

        public int mNumCorrect {
            get { return stats.FindAll(x => x == true).Count; }
        }

        public int mNumQuestions {
            get { return stats.Count; }
        }

        public bool toReset = false; // Set to true for new exercises to reset current scores.

        public void insert(bool result)
        {
            stats.Insert(0, result);
            if (stats.Count > GlobalSettings.maxQuestionsCount)
            {
                stats.RemoveAt(stats.Count - 1);
            }
        }

        public List<bool> stats = new List<bool>();
    }

    public class ExerciseModel
    {
        private bool _mBusy = true;
        private string _mCorrectAnswer = null;
        private bool _mLastAnswerCorrect = true;
        private bool _mActive = true;
        private bool _mQuestionUnanswered = false;
        private bool _mFirstQuestionIsSet = false;
        private int _mQsInCurrentKey = 0;
        private Key _mCurrentKey;
        //public QuestionAnswer mPreviousQuestionAnswer = null;
        public QuestionAnswer mCurrentQuestionAnswer = null;
        private ExerciseStats _mExerciseStats;
        public int mCurrentQuestionCount = 0;

        // Benefits of calling update in model: less programming errors/thinking overhad
        // Drawbacks: more performance costs (possibly group all calls to LateUpdate)
        public bool mBusy
        {
            get { return _mBusy; }
            set { _mBusy = value; mEventSource.OnUpdate(); }
        }

        public void updateCorrectness(bool correct)
        {
            _mExerciseStats.insert(correct);
        }

        public int mCorrect
        {
            get { return _mExerciseStats.mNumCorrect; }
        }

        public int mTotal
        {
            get { return _mExerciseStats.mNumQuestions; }
        }

        public bool mLastAnswerCorrect
        {
            get { return _mLastAnswerCorrect; }
            set { _mLastAnswerCorrect = value; mEventSource.OnUpdate(); }
        }

        public bool mActive
        {
            get { return _mActive; }
            set { _mActive = value; mEventSource.OnUpdate(); }
        }

        public bool mQuestionUnanswered
        {
            get { return _mQuestionUnanswered; }
            set { _mQuestionUnanswered = value; mEventSource.OnUpdate(); }
        }

        public bool mFirstQuestionIsSet
        {
            get { return _mFirstQuestionIsSet; }
            set { _mFirstQuestionIsSet = value; mEventSource.OnUpdate(); }
        }

        public int mQsInCurrentKey
        {
            get { return _mQsInCurrentKey; }
            set { _mQsInCurrentKey = value; mEventSource.OnUpdate(); }
        }

        public Key mCurrentKey
        {
            get { return _mCurrentKey; }
            set { _mCurrentKey = value; mEventSource.OnUpdate(); }
        }

        public EventSource mEventSource;

        public ExerciseModel(Key currentKey, QuestionAnswer lastQuestionAnswer)
        {
            _mCurrentKey = currentKey;
            mCurrentQuestionAnswer = lastQuestionAnswer;
        }

        public ExerciseModel(ExerciseStats stats) {
            mEventSource = new EventSource();
            _mExerciseStats = stats;
        }
    }

    public class ExerciseUtils
    {
        /* Anatomy of an exercise:
         * An exercise contains the following:
         * 
         * If new key:
         *      Play characteristic chord
         * 
         * A playableConstruct occurs
         * User picks and answer, this leads to a callback
         * It is correct or not correct
         * 
         * Rinse and repeat.
         * 
         * All exercises implement interfaces, the interfaces
         * enable interesting things to happen.
         * 
         * Static condition: answers must not change.
         */

        public static Sequence PlayCorrectJingle()
        {
            return DOTween.Sequence();
        }

        public static Sequence PlayIncorrectJingle()
        {
            return DOTween.Sequence();
        }
    
    }

    public class QuestionAnswer
    {
        public PlayableConstruct mQuestion;
        public string[] mAnswers;
        public QuestionAnswer(PlayableConstruct question, string answer)
        {
            mQuestion = question;
            mAnswers = new string[] { answer };
        }

        public QuestionAnswer(PlayableConstruct question, string[] answer)
        {
            mQuestion = question;
            mAnswers = answer;
        }
    }

    public interface ExerciseController
    {
        void Initialize(ExerciseAndStats exerciseAndStats);
        PlayableConstruct GetAnswerExample(string answer);
        void StartView();
        bool ProcessAnswer(string answer);
        void PreviewAnswer(string answer);
        QuestionAnswer ChooseNextQuestionAnswer();
        void SetNextQuestionAnswer();
        CorrectPolicy GetCorrectBehavior();
        bool GetNextQuestionHasKeySwitch(bool correct);
        IncorrectPolicy GetIncorrectBehavior();
        void SetIsBusy(bool busy);
        ExerciseModel GetModel();
        List<string> GetAnswers();
    }

    public interface ExerciseView
    {
        void Initialize(ExerciseModel model, ExerciseController ctrl);
        void UserAnswer(string answer);
        void PlayCorrectBehavior(CorrectPolicy policy);
        void PlayIncorrectBehavior(IncorrectPolicy policy);
        Sequence PlayPotentialAnswer(string answer, bool autoplay);
        Sequence ReplayQuestion(bool autoplay, bool setBusy = true);
        Sequence ReplayKey(bool autoplay, bool setBusy = true);
        void RefreshView();
    }
}
