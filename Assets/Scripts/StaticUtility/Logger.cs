using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Globals
{
    public static class Logger
    {
        public static List<string> mLog = new List<string>();

        public static void LogError(string error)
        {
            mLog.Add(error);
            Debug.LogError(error);
        }
    }
}
