﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PitchShifter : MonoBehaviour {

	AudioClip clip;

    private List<KeyCode> keys = new List<KeyCode>{
        KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.F, 
        KeyCode.G, KeyCode.H, KeyCode.J, KeyCode.K, 
        KeyCode.L, KeyCode.Semicolon, KeyCode.Quote, KeyCode.Return, 
        KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, 
        KeyCode.T, KeyCode.Y, KeyCode.U, KeyCode.I,
        KeyCode.O, KeyCode.P, KeyCode.LeftBracket, KeyCode.RightBracket, KeyCode.Slash};

	// Use this for initialization
	void Start () {
		clip = gameObject.GetComponent<AudioSource>().clip;
	}
	
	// Update is called once per frame
	void Update () {
		int shift = 0;

		foreach(KeyCode kc in keys){
			if(Input.GetKeyDown(kc)){
				GameObject newNote = new GameObject();
				AudioSource asc = newNote.AddComponent<AudioSource>();
				asc.pitch = Mathf.Pow(2f, shift / 12f);
				asc.clip = clip;
				asc.PlayOneShot(clip);
			}
			shift++;
		}

		//GetComponent<AudioSource>().pitch = 2f;
	}

	public static float GetPitchShift(float shift) {
		return Mathf.Pow(2f, shift / 12f);
	}
}
