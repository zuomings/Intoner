using UnityEngine;
using System.Collections;

namespace Exercise
{
    public class ExerciseMain : MonoBehaviour
    {
        ViewFrame mFrame;
        ExerciseController mController;

        // Use this for initialization
        void Start()
        {
            Globals.Initializer.Inialize();
        }

        public void Initialize(ExerciseAndStats exerciseAndStats)
        {
            // Get the controller type associated with the exercise
            System.Type t = ExerciseProperties.propertyToControllerMap[
                exerciseAndStats.mExerciseProperties.mControllerClass];
            mController = (ExerciseController)System.Activator.CreateInstance(t);

            mController.Initialize(exerciseAndStats);
            mController.StartView();

            gameObject.AddComponent<ClickManager>();
		}
	}
}