﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SaveLoad;

public class MainScript : MonoBehaviour {

    public Globals.DirectoryChooser directoryChooser;
	// Use this for initialization
	void Start () {
        //Screen.fullScreen = false;
        Globals.Initializer.Inialize();

        directoryChooser.setup(Globals.DirectoryChooser.OPENSAVE.OPEN);
        directoryChooser.callbackYes = delegate (string directoryName)
        {
            directoryChooser.gameObject.SetActive(false);
            if (Directory.Exists(directoryName))
            {
                OnDirectorySelected(directoryName);
            }
            else
            {
                Debug.LogError("The file does not exist: " + directoryName);
            }

        };
        directoryChooser.callbackNo = delegate () {
            directoryChooser.gameObject.SetActive(false);
        };
	}

	private void OnDirectorySelected(string directory)
    {
        SaveLoadManager.Reinitialize(directory);
        if (SaveLoadManager.GetSavedExercises().Count == 0)
        {
            // Create a completely new exercise if none has been created thus far.
            SaveLoadManager.SaveExercise(Exercise.ExerciseAndStats.Default, "Default");
            SaveLoadManager.Reinitialize(directory);
        }
        GameObject exercisePicker = new GameObject("ExercisePicker");
        exercisePicker.AddComponent<ExercisePicker>();
	}
}