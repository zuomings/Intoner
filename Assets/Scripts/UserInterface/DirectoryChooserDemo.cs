﻿/*
 *        This code should work on Windows Standalone/ Mac / Android /IOS
 * 		  but not WebPlayer or Windows 8 as these don't have direct access to the file system
 * 
 */
using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;


public class DirectoryChooserDemo : MonoBehaviour {
	 
    public Globals.DirectoryChooser directoryChooser;

	public void Start(){
        directoryChooser.setup (Globals.DirectoryChooser.OPENSAVE.OPEN);
		directoryChooser.callbackYes = delegate(string directoryName) 
        {
			directoryChooser.gameObject.SetActive (false);
            if (Directory.Exists(directoryName))
            {
				//load text file
				try
				{
                    Debug.Log("Directory " + directoryName);
				}
				catch (Exception)
				{
                    Debug.LogError("The directory could not be read: " + directoryName);
				}
			}
            else
            {
                Debug.LogError("The file does not exist: " + directoryName);
			}

		};
		directoryChooser.callbackNo = delegate() {
			directoryChooser.gameObject.SetActive (false);
		};
	}
}
