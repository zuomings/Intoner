using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

public class CallbackButtonScript : Clickable
{
    public string mText
    {
        get
        {
            return _mText;
        }
        set
        {
            _mText = value;
            textMeshObj.GetComponent<TextMesh>().text = _mText;
        }
    }

    public delegate void buttonCallback();
    private buttonCallback _mButtonCallback;
    private string _mText;
    private _Mono textMeshObj;

    public void Init(string text, buttonCallback callback)
	{
        _mText = text;
        _mButtonCallback = callback;
        nickname = text;

        textMeshObj = new GameObject("ButtonText").AddComponent<_Mono>();
        textMeshObj.transform.SetParent(transform, false);

        textMeshObj.xys = new Vector2(0.1f, 0.1f);
        textMeshObj.z = -1f;

        TextMesh mesh = textMeshObj.gameObject.AddComponent<TextMesh>();
        mesh.anchor = TextAnchor.MiddleCenter;
        mesh.alignment = TextAlignment.Center;
        mesh.text = _mText;
	}

    public void SetTextColor(Color c)
    {
        TextMesh mesh = textMeshObj.gameObject.GetComponent<TextMesh>();
        mesh.color = c;
    }

    public override void OnClick()
    {
        Debug.Log("Clicked on " + mText);
        _mButtonCallback();
    }
}