﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exercise;
using Globals;

namespace SaveLoad
{
    public class ExercisePicker : _Mono
    {
        void Start()
        {
            Dictionary<string, ExerciseAndStats> savedExercises = SaveLoadManager.GetSavedExercises();

            new GameObject("ViewFrame").AddComponent<ViewFrame>();

            GridLayoutManager layoutManager = new GameObject("LayoutManager").AddComponent<GridLayoutManager>();
            layoutManager.xys = new Vector2(1.0f, 1.0f);
            int buttonRows = Mathf.CeilToInt(savedExercises.Count / 2.0f);
            int buttonColumns = 2;

            layoutManager.Init(buttonRows, buttonColumns);
            Utils.GetViewFrame().Attach(layoutManager);

            var exerciseList = Globals.Utils.DictToList<string, ExerciseAndStats>(savedExercises);
            exerciseList.Sort((x, y) => {return x.Key.CompareTo(y.Key);});
            for (int i = 0; i < buttonRows * buttonColumns; i++)
            {
                _Mono button = Utils.MakeSpritedMonoWithCollider("CallbackButton");
                CallbackButtonScript callbackButton = button.gameObject.AddComponent<CallbackButtonScript>();
                //TODO: remember to customize the button
                //Can we do this via the constructor?
                if (i < savedExercises.Count)
                {
                    int total = exerciseList[i].Value.mExerciseStats.mNumQuestions;
                    int correct = exerciseList[i].Value.mExerciseStats.mNumCorrect;

                    int percentage = 90;
                    if (total != 0)
                    {
                        percentage = Mathf.RoundToInt((float)correct / (float)total * 100);
                    }
                                   
                    string progress = "";
                    if (total <= 20)
                    {
                        progress = correct + "/" + total;
                    }
                    else
                    {
                        progress = percentage + "%";
                    }

                    Color textColor = ColorsUtils.Red;
                    if (percentage > 95)
                    {
                        textColor = ColorsUtils.LightSkyBlue;
                    }
                    else if (percentage > 90)
                    {
                        textColor = ColorsUtils.Green;
                    }
                    else if (percentage > 80)
                    {
                        textColor = ColorsUtils.YellowGreen;
                    }
                    else if (percentage > 70)
                    {
                        textColor = ColorsUtils.Yellow;
                    }
                    else if (percentage > 60)
                    {
                        textColor = ColorsUtils.Orange;
                    }

                    var icached = i;
                    var exericiseName = exerciseList[i].Key;
                    var buttonName = exerciseList[i].Key + " " + progress;
                    callbackButton.Init(buttonName, () =>
                    {
                        Debug.Log("picked answer index " + icached);
                        LoadAndStartExercise(exericiseName);
                    });
                    callbackButton.SetTextColor(textColor);

                    layoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                else
                {
                    // Blank button
                    callbackButton.Init("", () => { });
                    layoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                //mListButtons.Add(callbackButton);
            }

            foreach (KeyValuePair<string, ExerciseAndStats> kvp in savedExercises)
            {
                //layoutManager.AddGulf(GlobalSettings.buttonGulf);
                layoutManager.SetTable();
            }
        }

        public void LoadAndStartExercise(string exerciseString)
        {
            // TODO: reflactor to reduce klutz code around main.
            DestroyImmediate(GameObject.Find("ViewFrame"));
            DestroyImmediate(GameObject.Find("Main"));
            SaveLoadManager.LoadExerciseWithoutStarting(exerciseString);
            ExerciseMain exerciseMain = new GameObject().AddComponent<ExerciseMain>();
            exerciseMain.gameObject.name = "ExerciseMain";
            exerciseMain.Initialize(SaveLoadManager.GetCurrentExercisePair().Value);
            DestroyImmediate(GameObject.Find("ExercisePicker"));
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}