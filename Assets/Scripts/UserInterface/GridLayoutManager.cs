﻿using System.Collections;
/****************************************************************************************
/	GridLayoutManager: arranges a set of child objects
/
/
****************************************************************************************/

using System.Collections.Generic;
using UnityEngine;

public class GridLayoutManager : _Mono {

	private _Mono[,] items;
	private Vector2[,] itemsRatio;

	// Use this for initialization
	void Start () {}
	
	public void Init (int rows, int cols) {
        items = new _Mono[cols, rows];
        itemsRatio = new Vector2[cols, rows];
        //xys = new Vector2(1.0f, 1.0f); // No transofrm object????
        //Globals.Utils.GetViewFrame().Attach(this);
        //ys = Camera.main.orthographicSize * 2;
        //xs = ys * Camera.main.aspect;
	}

	public void Insert (int row, int col, _Mono go, float xRatio = 1.0f, float yRatio = 1.0f) {
//      Debug.Log(row + ", " + col);
        items[col, row] = go;
        go.transform.SetParent(transform, false);
        itemsRatio[col, row] = new Vector2(xRatio, yRatio);
	}

    public void Commit () {
        //AddGulf(Globals.GlobalSettings.buttonGulf);
        SetTable();
    }

    // Assumes that there has been no padding added.
    public void AddGulf (float gulfSize) {
        int cols = items.GetLength(0);
        int rows = items.GetLength(1);
        _Mono[,] newItems = new _Mono[cols * 2 - 1, rows * 2 - 1];
        Vector2[,] newRatios = new Vector2[cols * 2 - 1, rows * 2 - 1];
        for (int i = 0; i < cols * 2 - 1; i++) 
        {
            for (int j = 0; j < rows * 2 - 1; j++)
            {
                if (i % 2 == 0 && j % 2 == 0)
                {
                    newItems[i, j] = items[i / 2, j / 2];
                    newRatios[i, j] = itemsRatio[i / 2, j / 2];
                }
                else if (i % 2 != 0 && j % 2 == 0)
                {
                    // Vertical stripes
                    newItems[i, j] = new GameObject("gulf").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(gulfSize, itemsRatio[i / 2, 0].y);
                }
                else if (i % 2 == 0 && j % 2 != 0)
                {
                    // Horizontal stripes
                    newItems[i, j] = new GameObject("gulf").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(itemsRatio[0, j / 2].x, gulfSize);
                }
                else
                {
                    newItems[i, j] = new GameObject("gulf").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(gulfSize, gulfSize);
                }
                newItems[i, j].transform.SetParent(this.transform, false);
                //Debug.Log(newRatios[i, j]);
            }
        }
        items = newItems;
        itemsRatio = newRatios;
    }

    public void AddBorders (float borderSize) {
        int cols = items.GetLength(0);
        int rows = items.GetLength(1);
        _Mono[,] newItems = new _Mono[cols + 2, rows + 2];
        Vector2[,] newRatios = new Vector2[cols + 2, rows + 2];

        Debug.Log("Rows:" + rows + ", Cols: " + cols);
        for (int i = 0; i < cols + 2; i++)
        {
            for (int j = 0; j < rows + 2; j++)
            {
                Debug.Log("i: " + i + "j: " + j);
                if ((i == 0 || i == cols + 1) && (j == 0 || j == rows + 1))
                { // Corners
                    newItems[i, j] = new GameObject("border").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(borderSize, borderSize);
                }
                else if (j == 0 || j == rows + 1)
                {   
                    // Horizontal
                    // Precondition (j != 0 && j != cols + 1)
                    newItems[i, j] = new GameObject("border").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(itemsRatio[i - 1, 0].x, borderSize);
                }
                else if (i == 0 || i == cols + 1)
                {   
                    // Vertical
                    // Precondition (i != 0 && i != rows + 1)
                    newItems[i, j] = new GameObject("border").AddComponent<_Mono>();
                    newRatios[i, j] = new Vector2(borderSize, itemsRatio[0, j - 1].y);
                }
                else
                {
                    newItems[i, j] = items[i - 1, j - 1];
                    newRatios[i, j] = itemsRatio[i - 1, j - 1];
                }
                newItems[i, j].gameObject.AddComponent<BoxCollider>();
                newItems[i, j].transform.SetParent(this.transform, false);
            }
        }

        items = newItems;
        itemsRatio = newRatios;
    }

    public void LogRatios()
    {
        string result = "";
        for (int j = 0; j < items.GetLength(1); j++)
        {
            for (int i = 0; i < items.GetLength(0); i++)
            {
                result += "(" +
                    string.Format("{0:0.00}", itemsRatio[i, j].x) +
                          ", " + string.Format("{0:0.00}", itemsRatio[i, j].y) +
                    "), ";
            }
        }
        result += "\n";
        Debug.Log(result);

        result = "";
        for (int j = 0; j < items.GetLength(1); j++)
        {
            for (int i = 0; i < items.GetLength(0); i++)
            {
                result += "(" + 
                    string.Format("{0:0.00}", items[i, j].xs) + 
                          ", "+ string.Format("{0:0.00}", items[i, j].ys) + 
                    "), ";
            }
        }
        result += "\n";
        Debug.Log(result);

        result = "";
        for (int j = 0; j < items.GetLength(1); j++)
        {
            for (int i = 0; i < items.GetLength(0); i++)
            {
                result += "(" +
                    string.Format("{0:0.00}", items[i, j].x) +
                          ", " + string.Format("{0:0.00}", items[i, j].y) +
                    "), ";
            }
        }
        result += "\n";
        Debug.Log(result);
    }

    // Arranges position and size of all elements of the table appropriately.
    public void SetTable (){
        for (int i = 0; i < items.GetLength(0); i++) {
            float totalSize = 0;
            float[] prevYs = new float[items.GetLength(1)];
            //prevXs[j] -= 0.5f;
            for (int j = 0; j < items.GetLength(1); j++) {
                totalSize += itemsRatio[i, j].y;
                if (j > 0)
                {
//                    Debug.Log(i + ", " + j);
                    prevYs[j] = prevYs[j - 1] + itemsRatio[i, j - 1].y;
                }
            }

            for (int j = 0; j < items.GetLength(1); j++)
            {
                if (totalSize == 0f)
                {
                    break;
                }

                // This part kind of makes sense if you are talking about top-left centered, but falls apart otherwise.
                items[i, j].y = -(prevYs[j] / totalSize - 0.5f + itemsRatio[i, j].y / totalSize / 2f);
                items[i, j].ys = itemsRatio[i, j].y / totalSize;
            }
        }

        // 0.1 1 0.1 1 0.1
        // totalsize = 2.3
        // prevxs = 0, 0.1, 1.1, 1.2, 2.2
        // 0 / 2.3 - 0.5 + 0.1 / 2.3 / 2.
        // 0.1 / 2.3 - 0.5 

        for (int j = 0; j < items.GetLength(1); j++) {
            float totalSize = 0;
            float[] prevXs = new float[items.GetLength(0)];
            //prevYs[0] -= 0.5f;
            for (int i = 0; i < items.GetLength(0); i++)
            {
                totalSize += itemsRatio[i, j].x;
                if (i > 0)
                {
                    prevXs[i] = prevXs[i - 1] + itemsRatio[i - 1, j].x;
                }
            }

            for (int i = 0; i < items.GetLength(0); i++)
            {
                if (totalSize == 0f)
                {
                    break;
                }
                items[i, j].x = prevXs[i] / totalSize - 0.5f + itemsRatio[i, j].x / totalSize / 2f;
                items[i, j].xs = itemsRatio[i, j].x / totalSize;
                Debug.Log("Test " + prevXs[i] + " " + totalSize + " " + itemsRatio[i, j].x + " " + items[i, j].x);
            }
        }
        LogRatios();
    }
}
