using UnityEngine;
using System.Collections;

public class TextBox : _Mono
{
    public string mText
    {
        get
        {
            return _mText;
        }
        set
        {
            _mText = value;
            textMeshObj.GetComponent<TextMesh>().text = _mText;
        }
    }

    private string _mText;
    private GameObject textMeshObj;

    public void Init(string text, Vector2 _xy, Vector2 _xys)
    {
        _mText = text;

        textMeshObj = new GameObject("TextBox");
        textMeshObj.transform.SetParent(transform, false);
        TextMesh mesh = textMeshObj.AddComponent<TextMesh>();
        mesh.alignment = TextAlignment.Left;
        mesh.anchor = TextAnchor.MiddleLeft;
        _Mono textMeshMono = textMeshObj.AddComponent<_Mono>();
        textMeshMono.xy = _xy;
        textMeshMono.xys = _xys;
        textMeshMono.z = -1.0f;
        mesh.text = _mText;
    }

    public void SetTextColor(Color c)
    {
        TextMesh mesh = textMeshObj.gameObject.GetComponent<TextMesh>();
        mesh.color = c;
    }
}
