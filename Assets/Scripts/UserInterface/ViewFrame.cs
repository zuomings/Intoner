﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewFrame : _Mono {

    private class ChildObj {
        
    }

	// Use this for initialization
	void Start () {
        name = "ViewFrame";
        xyz = new Vector3(0f, 0f, 0f);
        xys = new Vector3(
            Camera.main.orthographicSize * 2 * Camera.main.aspect, 
            Camera.main.orthographicSize * 2
        );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Attach(_Mono mono)
    {
        mono.transform.SetParent(transform, false);
    }

    void Attach (_Mono mono, Vector2 _xy, Vector2 _xys)
    {
        mono.transform.SetParent(transform, false);
        mono.xys = _xys;
    }
}