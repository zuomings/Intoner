﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

public class AnswerButtonScript : Clickable {

    public string mAnswer { 
        get {
            return _mAnswer;
        }
        set {
            _mAnswer = value;
            gameObject.GetComponent<TextMesh>().text = _mAnswer;
        } 
    }

    private string _mAnswer;
    public Exercise.ExerciseView mExercise;
    public _Mono textMeshObj;

    public void Initialize(string answer, Exercise.ExerciseView exercise) {
        mExercise = exercise;
        mAnswer = answer;

        textMeshObj = new GameObject("ButtonText").AddComponent<_Mono>();
        textMeshObj.xys = new Vector2(0.1f, 0.1f);
        textMeshObj.z = -1f;
        TextMesh mesh = textMeshObj.gameObject.AddComponent<TextMesh>();
        mesh.anchor = TextAnchor.MiddleCenter;
        mesh.alignment = TextAlignment.Center;
        mesh.text = answer;
    }

	public override void OnClick() {
        Debug.Log("Clicked on " + mAnswer);
        mExercise.UserAnswer(mAnswer);
	}
}