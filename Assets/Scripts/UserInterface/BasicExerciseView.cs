using UnityEngine;
using System.Collections;
using Exercise;
using System.Collections.Generic;
using Globals;
using DG.Tweening;

namespace Exercise
{
    public class BasicExerciseView : _Mono, ExerciseView
    {
        ExerciseModel mModel;
        ExerciseController mController;
        EventListener mEvtList;
        GridLayoutManager mLayoutManager;
        List<CallbackButtonScript> mListButtons;
        const string REPLAY_ALL_BUTTON_NAME = "Replay All";
        const string NEXT_QUESTION_BUTTON_NAME = "Next Question";

        TextBox scoreText;
        TextBox keyText;
        TextBox timeText;
        
        // Use this for initialization
        void Start() {; }

        public void Initialize(ExerciseModel model, ExerciseController ctrl)
        {
            mListButtons = new List<CallbackButtonScript>();

            new GameObject("ViewFrame").AddComponent<ViewFrame>();
            mModel = model;
            mController = ctrl;
            mEvtList = new EventListener(() => {
                RefreshView();
            });
            mModel.mEventSource.Subscribe(mEvtList);

            mLayoutManager = new GameObject("LayoutManager").AddComponent<GridLayoutManager>();
            mLayoutManager.xys = new Vector2(1.0f, 1.0f);
            int buttonRows = Mathf.CeilToInt(ctrl.GetAnswers().Count / 2.0f);
            int buttonColumns = 2;
            mLayoutManager.Init(buttonRows + 1, buttonColumns);
            Utils.GetViewFrame().Attach(mLayoutManager);
            var answers = mController.GetAnswers();
            for (int i = 0; i < (buttonRows + 1) * buttonColumns; i++)
            {
                _Mono button = Utils.MakeSpritedMonoWithCollider("CallbackButton");
                CallbackButtonScript callbackButton = button.gameObject.AddComponent<CallbackButtonScript>();
                callbackButton.spriteRenderer.color = ColorsUtils.LightSeaGreen;
                //TODO: remember to customize the button
                //Can we do this via the constructor?

                // Where does the different button behavior belong? Contoller, view, button?
                // View knows how to talk to button.
                // The controller should know nothing about what the view is doing, or is this accurate?
                // Suppose the view is aware of the model state, and chooses which controller functions to call?
                // Suppose the controller only can modify the model?

                // Controller changes the model, the view listens to the model, the view is in charge of which of various controller interfaces to call.
                if (i < answers.Count)
                {
                    var icached = i;
                    callbackButton.Init(answers[i], ()=>{
                        Debug.Log("picked answer index " + icached);
                        if (GlobalSettings.practiceMode == GlobalSettings.PracticeStrategy.MULTIPLE_GUESSES)
                        {
                            UserAnswer(answers[icached]);
                        }
                        else
                        {
                            if (mModel.mQuestionUnanswered)
                            {
                                UserAnswer(answers[icached]);
                            }
                            else
                            {
                                UserPreview(answers[icached]);
                            }
                        }
                    });
                    mLayoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                else if (i == buttonRows * buttonColumns)
                {
                    if (GlobalSettings.practiceMode == GlobalSettings.PracticeStrategy.MULTIPLE_GUESSES)
                    {
                        callbackButton.Init("Replay Question", () =>
                        {
                            if (!mModel.mBusy)
                            {
                                ReplayQuestion(true);
                            }
                        });
                    }
                    else
                    {
                        callbackButton.Init(REPLAY_ALL_BUTTON_NAME, () =>
                        {
                            if (!mModel.mBusy)
                            {
                                ReplayKeyAndQuestion(true);
                            }
                        });
                    }
                    mLayoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                else if (i == buttonRows * buttonColumns + 1)
                {
                    if (GlobalSettings.practiceMode == GlobalSettings.PracticeStrategy.MULTIPLE_GUESSES)
                    {
                        callbackButton.Init("Replay Key", () =>
                        {
                            if (!mModel.mBusy)
                            {
                                ReplayKey(true);
                            }
                        });
                    }
                    else
                    {
                        callbackButton.Init(NEXT_QUESTION_BUTTON_NAME, () =>
                        {
                            if (!mModel.mBusy)
                            {
                                if (!mModel.mQuestionUnanswered)
                                {
                                    PlayCorrectBehavior(CorrectPolicy.NEXT);
                                }
                            }
                        });
                    }
                    mLayoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                else
                {
                    // Blank button
                    callbackButton.Init("", () => {});
                    mLayoutManager.Insert(
                        i / buttonColumns,
                        i % buttonColumns,
                        callbackButton);
                }
                mListButtons.Add(callbackButton);
            }

            mLayoutManager.AddGulf(GlobalSettings.buttonGulf);
            mLayoutManager.SetTable();
            mLayoutManager.AddBorders(GlobalSettings.buttonGulf);
            mLayoutManager.SetTable();

            // Scoring text
            scoreText = Utils.MakeSpritedMono("ScoreText").gameObject.AddComponent<TextBox>();
            scoreText.spriteRenderer.enabled = false;
            scoreText.Init("9/10", new Vector2(0.32f, 0.40f), new Vector2(0.05f, 0.04f));
            scoreText.transform.SetParent(mLayoutManager.transform);

            keyText = Utils.MakeSpritedMono("KeyText").gameObject.AddComponent<TextBox>();
            keyText.spriteRenderer.enabled = false;
            keyText.Init("C Maj", new Vector2(0.32f, 0.45f), new Vector2(0.05f, 0.04f));
            keyText.transform.SetParent(mLayoutManager.transform);

            timeText = Utils.MakeSpritedMono("TimeText").gameObject.AddComponent<TextBox>();
            timeText.spriteRenderer.enabled = false;
            timeText.Init("00:00", new Vector2(0.32f, 0.35f), new Vector2(0.05f, 0.04f));
            timeText.transform.SetParent(mLayoutManager.transform);

            Sequence sequence = DOTween.Sequence();
            sequence.Append(AudioUtils.Play(
                GlobalSettings.correctSound,
                GlobalSettings.correctJingleDuration));
            sequence.Play();
        }

        // Update is called once per frame
        void Update()
        {
            System.TimeSpan timeSpan = (System.DateTime.Now - GlobalSettings.startTime);
            timeText.mText = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
        }

        public void UserPreview(string answer)
        {
            mController.PreviewAnswer(answer);
        }

        public void UserAnswer(string answer)
        {
            Utils.LogMethodEntry();
            if (!mModel.mBusy)
            {
                bool result = mController.ProcessAnswer(answer);
                if (result)
                {
                    PlayCorrectBehavior(mController.GetCorrectBehavior());
                }
                else
                {
                    PlayIncorrectBehavior(mController.GetIncorrectBehavior());
                }
            }
        }

        public void PlayCorrectBehavior(CorrectPolicy policy)
        {
            Utils.LogMethodEntry();
            mController.SetNextQuestionAnswer();
            Sequence sequence = DOTween.Sequence();
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));

            switch (policy)
            {
                case CorrectPolicy.JINGLE:
                    Debug.Log("Jingle");
                    sequence.Append(AudioUtils.Play(
                        GlobalSettings.correctSound,
                        GlobalSettings.correctJingleDuration));
                    break;
                case CorrectPolicy.NEXT:
                    break;
                default:
                    break;
            }

            //sequence.Append(Utils.MakeOrderedSequence(() => mController.SetNextQuestionAnswer()));

            //Key currentKey = mModel.mCurrentKey;
            if (mController.GetNextQuestionHasKeySwitch(true))
            {
                sequence.Append(ReplayKey(false));
            }

            sequence.Append(ReplayQuestion(false));
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));
            sequence.Play();
        }

        public void PlayIncorrectBehavior(IncorrectPolicy policy)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));
            switch (policy)
            {
                case IncorrectPolicy.BOO:
                    Debug.Log("Boo"); 
                    sequence.Append(AudioUtils.Play(
                        GlobalSettings.incorrectSound,
                        GlobalSettings.incorrectJingleDuration));
                    break;
                case IncorrectPolicy.BOO_REPLAY:
                    Debug.Log("Boo_Replay");
                    sequence.Append(AudioUtils.Play(
                        GlobalSettings.incorrectSound,
                        GlobalSettings.incorrectJingleDuration));
                    sequence.Append(ReplayQuestion(false));
                    break;
                default:
                    break;
            }
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));
        }

        public Sequence PlayPotentialAnswer(string answer, bool autoPlay)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));
            sequence.Append(AudioUtils.Play(mController.GetAnswerExample(answer)));
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));

            if (autoPlay)
            {
                sequence.Play();
            }
            return sequence;
        }

        public Sequence ReplayQuestion(bool autoPlay, bool setBusy = true)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            if (setBusy)
            {
                sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));
            }
            sequence.Append(AudioUtils.Play(mModel.mCurrentQuestionAnswer.mQuestion));
            if (setBusy)
            {
                sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));
            }

            if (autoPlay)
            {
                sequence.Play();
            }
            return sequence;
        }

        public Sequence ReplayKey(bool autoPlay, bool setBusy = true)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            if (setBusy)
            {
                sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));
            }
            sequence.Append(AudioUtils.Play(mModel.mCurrentKey.GetCenteralChordProgression()));
            if (setBusy)
            {
                sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));
            }

            if (autoPlay)
            {
                sequence.Play();
            }
            return sequence;
        }

        public Sequence ReplayKeyAndQuestion(bool autoPlay)
        {
            Utils.LogMethodEntry();
            Sequence sequence = DOTween.Sequence();
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(true)));
            sequence.Append(ReplayKey(false, false));
            sequence.Append(ReplayQuestion(false, false));
            sequence.Append(Utils.MakeOrderedSequence(() => mController.SetIsBusy(false)));

            if (autoPlay)
            {
                sequence.Play();
            }
            return sequence;
        }

        public void RefreshView()
        {
            mListButtons.ForEach(button =>
            {
                Color color = mController.GetModel().mBusy ? Color.gray :
                                         Color.white;
                button.SetTextColor(color);

                if (mController.GetModel().mQuestionUnanswered && button.nickname.Equals(NEXT_QUESTION_BUTTON_NAME))
                {
                    button.SetTextColor(Color.gray);
                }
            });

            ExerciseModel model = mController.GetModel();

            keyText.mText = model.mCurrentKey.mDisplayName;

            if (model.mTotal == 0)
            {
                scoreText.mText = model.mCorrect + "/" + mController.GetModel().mTotal;
            } else {
                Color textColor = ColorsUtils.Red;
                int percentage = Mathf.RoundToInt((float)model.mCorrect / (float)model.mTotal * 100);
                if (percentage > 95)
                {
                    textColor = ColorsUtils.LightSkyBlue;
                }
                else if (percentage > 90)
                {
                    textColor = ColorsUtils.Green;
                }
                else if (percentage > 80)
                {
                    textColor = ColorsUtils.YellowGreen;
                }
                else if (percentage > 70)
                {
                    textColor = ColorsUtils.Yellow;
                }
                else if (percentage > 60)
                {
                    textColor = ColorsUtils.Orange;
                }

                if (model.mTotal <= 20)
                {
                    scoreText.mText = model.mCorrect + "/" + mController.GetModel().mTotal;
                } else {
                    scoreText.mText = percentage + "%";
                }

                scoreText.SetTextColor(textColor);
            }
        }
    }
}