using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Exercise;
using DG.Tweening;
using Globals;
using System.Linq;
using SaveLoad;

namespace Exercise
{
    public class DegreeExerciseController : ExerciseController
    {
        ExerciseModel mModel;
        DegreeExerciseProperties mProperties;
        ExerciseView mView;
        HashSet<Key> mKeys;
        List<string> mAnswers; // List used since sortedSet not supported, and Set has no order.
        Dictionary<string, HashSet<PlayableConstruct>> mAnswerExamplePairs;

        public void Initialize(ExerciseAndStats exerciseAndStats)
        {
            ExerciseProperties exerciseProperties = exerciseAndStats.mExerciseProperties;
            mModel = new ExerciseModel(exerciseAndStats.mExerciseStats);
            if (exerciseProperties is DegreeExerciseProperties)
            {
                mProperties = exerciseProperties as DegreeExerciseProperties;
                var query = from pitchClass in mProperties.mTonics
                            from scale in mProperties.mScaleQuality
                            select new Key(pitchClass, scale);
                mKeys = new HashSet<Key>(query.ToList());

                mAnswers = new List<string>();
                mAnswerExamplePairs = new Dictionary<string, HashSet<PlayableConstruct>>();

                //mModel.mLastQuestionAnswer = ChooseNextQuestionAnswer();
                SetNextQuestionAnswer();
            }
        }

        public void StartView()
        {
            mView = new GameObject("BasicExerciseView").AddComponent<BasicExerciseView>();
            mView.Initialize(mModel, this);
            Sequence sequence = DOTween.Sequence();
            sequence.Append(mView.ReplayKey(false));
            sequence.Append(mView.ReplayQuestion(false));

            //AudioUtils.Play(new RealizedChord(PitchUtils.centerA, Chord.MAJ)).Play();
        }

        public void PreviewAnswer(string answer)
        {
            Utils.LogMethodEntry();
            bool isCorrect = this.mModel.mCurrentQuestionAnswer.mAnswers.Contains(answer);

            if (isCorrect)
            {
                mView.ReplayQuestion(true);
            }
            else
            {
                mView.PlayPotentialAnswer(answer, true);
            }
        }

        public bool ProcessAnswer(string answer)
        {
            Utils.LogMethodEntry();
            bool isCorrect = this.mModel.mCurrentQuestionAnswer.mAnswers.Contains(answer);

            if (isCorrect)
            {
                if (mModel.mQuestionUnanswered)
                {
                    // mLastAnswerCorrect indicates that there was no previous answer
                    // or last answer is good. But in the latter case we would not arrive
                    // at this method again, so this means that the user has guessed correctly.

                    //mModel.mTotal++;
                    //mModel.mCorrect++;
                    mModel.mCurrentQuestionCount++;
                    mModel.updateCorrectness(true);
                }
                mModel.mLastAnswerCorrect = true;
            }
            else if (mModel.mQuestionUnanswered)
            {
                //mModel.mTotal++;
                mModel.mCurrentQuestionCount++;
                mModel.updateCorrectness(false);
                mModel.mLastAnswerCorrect = false;
            }

            if (mModel.mQuestionUnanswered)
            {
                mModel.mQuestionUnanswered = false;
            }

            return isCorrect;
        }

        public List<string> GetAnswers()
        {
            return mAnswers;
        }

        public bool GetNextQuestionHasKeySwitch(bool lastQuestionCorrect)
        {
            return ((mModel.mCurrentQuestionCount) % mProperties.mInterationsTillKeySwitch == 0);
        }

        public QuestionAnswer ChooseNextQuestionAnswer()
        {
            Utils.LogMethodEntry();
            if (mModel.mQuestionUnanswered)
            {
                Debug.LogError("Asked to choose next question while current one is uanswered.");
                return null;
            }

            if (mModel.mCurrentQuestionCount % mProperties.mInterationsTillKeySwitch == 0 || mAnswers.Count == 0)
            {
                SaveProgress();
                mModel.mCurrentKey = Utils.GetRandom(mKeys.ToList());
                mAnswers.Clear();
                mAnswerExamplePairs.Clear();
                mProperties.mDegrees.ForEach(degInt =>
                {
                    mAnswers.Add(HarmonicUtils.degreeNames[degInt]);
                    //Debug.Log("" + PitchUtils.centerAIndex + " " + 
                    //          (int)mModel.mCurrentKey.mTonic + " " +
                    //         degInt);

                    // Pregenerate all answers or not??? Pregenerate all answers, instead of preplaying answers.
                    // The AnswerExamplePairs should include as answers an array of possible ans.
                    HashSet<PlayableConstruct> listExample = new HashSet<PlayableConstruct>();
                    mProperties.mAnswerOctaves.ForEach(o =>
                    {
                        listExample.Add(
                            new Pitch(PitchUtils.centerAIndex +
                                      (int)mModel.mCurrentKey.mTonic +
                                      mModel.mCurrentKey.mScaleIntervals.mNotes[degInt] +
                                      o * 12)
                        );
                    });
                    mAnswerExamplePairs.Add(
                        HarmonicUtils.degreeNames[degInt],
                        listExample
                    );
                });
            }

            var answer = Utils.GetRandom(mAnswers);
            PlayableConstruct question = null;
            question = Utils.GetRandom(mAnswerExamplePairs[answer].ToList());
            if (!GlobalSettings.CanRepeatSameNote && mModel.mFirstQuestionIsSet)
            {
                var previousQuestion = mModel.mCurrentQuestionAnswer.mQuestion;
                var previousKey = mModel.mCurrentKey;
                while (mAnswerExamplePairs[answer].Count > 1 && question == previousQuestion && previousKey == mModel.mCurrentKey)
                {
                    question = Utils.GetRandom(mAnswerExamplePairs[answer].ToList());
                }
            }
            var questionAnswer = new QuestionAnswer(question, answer);
            mModel.mQuestionUnanswered = true;
            mModel.mFirstQuestionIsSet = true;
            return questionAnswer;
        }

        public void SetNextQuestionAnswer() 
        {
            Utils.LogMethodEntry();
            //mModel.mPreviousQuestionAnswer = mModel.mCurrentQuestionAnswer;
            mModel.mCurrentQuestionAnswer = ChooseNextQuestionAnswer();
        }

        public CorrectPolicy GetCorrectBehavior()
        {
            return mProperties.correctPolicy;
        }

        public IncorrectPolicy GetIncorrectBehavior()
        {
            return mProperties.incorrectPolicy;
        }

        public PlayableConstruct GetAnswerExample(string answer)
        {
            if (!GlobalSettings.PickCloserNoteToPreview)
            {
                return mAnswerExamplePairs[answer].ToList()[Utils.RandomInt(mAnswerExamplePairs[answer].Count)];
            }
            // Note: relies on deterministic note generation order
            // PickCloserNoteToPreview
            var exampleList = mAnswerExamplePairs[answer].ToList();
            var minDist = int.MaxValue;
            PlayableConstruct minExample = null;
            var questionPitch = mModel.mCurrentQuestionAnswer.mQuestion.GetChart().mNotes[0].mPitch;
            for (int i = 0; i < exampleList.Count; i++)
            {
                int difference = Mathf.Abs(exampleList[i].GetChart().mNotes[0].mPitch.Subtract(questionPitch));
                if (difference < minDist)
                {
                    minDist = difference;
                    minExample = exampleList[i];
                }
            }

            return minExample;
        }

        public void SetIsBusy(bool busy)
        {
            Debug.Log("SetIsBusy: " + busy);
            // Will not accept user input
            mModel.mBusy = busy;
        }

        public ExerciseModel GetModel()
        {
            return mModel;
        }

        private void SaveProgress()
        {
            var currentExercisePair = SaveLoadManager.GetCurrentExercisePair();
            SaveLoadManager.SaveExercise(currentExercisePair.Value, currentExercisePair.Key);
        }
    }
}

/*
 * Model view controller
 * Controller respond to user events.
 * User events updates model.
 * Model updates view.
 * 
 * Model can be included as a part of the controller, and passed to view.
 */