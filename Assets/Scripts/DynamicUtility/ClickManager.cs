﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour {

	public void Update(){
		if(Input.GetMouseButtonDown(0)){
			Clickable clickedObj = Click(Input.mousePosition);
            if (clickedObj != null)
            {
                clickedObj.OnClick();
            }
		}
	}

	public Clickable Click(Vector3 mouseLocation){
		//Debug.Log("Click at " + xpos + ", " + ypos);
		//Instantiate(clickWave, xy, Quaternion.identity);
		Clickable clickObj = Scout(mouseLocation);
		if(clickObj != null){
			Debug.Log("Clicked on "+ clickObj.nickname);
		}
		return clickObj;
	}

	public Clickable Scout(Vector3 mouseLocation){
		Vector3 clickLoc = Camera.main.ScreenToWorldPoint(new Vector3(mouseLocation.x, mouseLocation.y, 10.0f));

		RaycastHit hitInfo;
		Physics.Raycast(new Vector3(clickLoc.x, clickLoc.y, 10000), new Vector3(0,0,-1), out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Clickable"));
		if(hitInfo.collider == null){
			Debug.Log("null");
			return null;
		}else{
			if(hitInfo.collider.GetComponent<Clickable>() == null){
				Debug.LogError("Collider did not have a clickable object");
			}
			return hitInfo.collider.GetComponent<Clickable>();
		}
	}
}
