﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDescructScript : MonoBehaviour {

	float maxLifetime = 5.0f;
	float currentLifetime = 0.0f;

	// Update is called once per frame
	void Update () {
		currentLifetime += Time.deltaTime;
		if (currentLifetime > maxLifetime) {
			Destroy(gameObject);
		}
	}
}
