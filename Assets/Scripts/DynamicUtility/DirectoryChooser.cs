﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;
using System.Text.RegularExpressions;

// TODO: modify this to focus on directories and be non-cancellable, then pass selected directory to SaveLoadManager.
// perhaps also store fileInput.text locally?
namespace Globals
{
    public class DirectoryChooser : MonoBehaviour
    {
        public GameObject titleObject;
        public GameObject button;
        public GameObject fileListPanel;

        public InputField fileInput;
        public Text placeholder;
        public Button openSaveButton;

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
	public static string SLASH="\\";
#else
        public static string SLASH = "/";
#endif

        string dir = "";
        string filter = "";
        string lastfilter = "";

        void Start()
        {
            if (dir == "")
            {
                SetStartingDirectory();
                showDirectories();
            }
        }

        void SetStartingDirectory()
        {
            //Sets the directory to the current directory
            //dir = Directory.GetCurrentDirectory();

            //Sets the directory to the documents folder 
            dir = Application.persistentDataPath;//Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
#if UNITY_ANDROID
            //dir = dir + "/com.dropbox.android/files/u113527442/scratch/IntonerExercises"
            //dir = dir.Substring(dir.Length - 28) + "/com.dropbox.android/files/u113527442/scratch/IntonerExercises";
            // /data/zone.zuoming.intoner/files/
            //27
            /*
            Original original place
            / storage / emulated / 0 / Android / data / zone.zuoming.intoner / files /

            Wrong ver
            a / zone.zuoming.intoner / file / com.dropbox.android / files / i113527442 / scratch / IntonerExercises

            New
            / storage / emulated / 0 / Android / data / com.dropbox.android / files / u113527442 / scratch / IntonerExercises / (I think ?)
            */
            dir = "/storage/emulated/0/Android/data/com.dropbox.android/files/u113527442/scratch/IntonerExercises";
#endif
        }

        public enum OPENSAVE { OPEN, SAVE };

        OPENSAVE openSave = OPENSAVE.OPEN;

        public void setup(OPENSAVE os)
        {
            openSave = os;

            //set text on buttons
            if (openSave == OPENSAVE.SAVE)
                openSaveButton.transform.Find("Text").GetComponent<Text>().text = "Save";
            if (openSave == OPENSAVE.OPEN)
                openSaveButton.transform.Find("Text").GetComponent<Text>().text = "Open";
            
            placeholder.text = "";
            Show(true);
        }

        public void Show(bool show)
        {
            fileInput.text = "";
            if (dir == "") SetStartingDirectory();
            showDirectories();
            gameObject.SetActive(true);
        }

        public void EndEdit()
        {
            if (fileInput.text.Length > 0 && (fileInput.text[fileInput.text.Length - 1] == '/'
                                            || fileInput.text[fileInput.text.Length - 1] == '\\'))
            {
                //remove last character
                fileInput.text = fileInput.text.Substring(0, fileInput.text.Length - 1);
            }

            if (fileInput.text != "" && Directory.Exists(dir + SLASH + fileInput.text))
            {
                dir = dir + SLASH + fileInput.text;
                fileInput.text = "";
                showDirectories();
                return;
            }

            if (filter == "")
            {
                Yes();
            }

            if (filter != lastfilter) showDirectories();
            lastfilter = filter;
        }

        void showDirectories()
        {
            //remove all buttons 
            foreach (Transform T in fileListPanel.transform)
            {
                Destroy(T.gameObject);
            }

            setTitle(dir + SLASH);
            string[] dirs = Directory.GetDirectories(dir);
            for (int i = 0; i < dirs.Length; i++)
            {
                GameObject newButton = (GameObject)Instantiate(button);
                newButton.transform.Find("Text").GetComponent<Text>().text =
                    Path.GetFileName(dirs[i]) + SLASH;
                newButton.transform.SetParent(fileListPanel.transform);
                newButton.transform.localScale = Vector3.one;
                newButton.GetComponentInChildren<Text>().color = Color.green;

                int j = i;
                Button.ButtonClickedEvent BCE = new Button.ButtonClickedEvent();
                BCE.AddListener(() =>
                {
                    dir = dirs[j];
                    Debug.Log("Current dir: " + dir);
                    showDirectories();
                });
                newButton.GetComponent<Button>().onClick = BCE;
            }
        }

        public void upDirectory()
        {
            Debug.Log("updir at: " + dir);
            if (Directory.GetParent(dir) != null)
            {
                dir = Directory.GetParent(dir).FullName;
                showDirectories();
            }
        }

        public void setTitle(string text)
        {
            titleObject.GetComponent<Text>().text = text;
        }

        //for load, loop through extensions. Don't return if file doesn't exist.
        //test on Mac

        public void Yes()
        {
            string fullFilename = dir + SLASH + fileInput.text;
            //add the first extension in the list if none specified
            if (openSave == OPENSAVE.SAVE)
            {
                /*
                if (!fileInput.text.Contains(".") && extensions.Count > 0)
                {
                    fullFilename += extensions[0];
                }
                */
                Debug.LogError("Not Implemented");
            }
            else
            {
                /*
                //if not extension provided try all the extensions
                if (!fileInput.text.Contains(".") && extensions.Count > 0)
                {
                    for (int i = 0; i < extensions.Count; i++)
                    {
                        string testFilename = fullFilename + extensions[i];
                        if (File.Exists(testFilename))
                        {
                            fullFilename = testFilename;
                            break;
                        }
                    }
                }
                //if file still not exist don't return
                if (!File.Exists(fullFilename))
                {
                    Debug.Log("Can't return. File does not exist: " + fullFilename);
                    return;
                }*/

                if (!Directory.Exists(fullFilename))
                {
                    Debug.LogError("Can't return. Directory does not exist: " + fullFilename);
                }
            }

            if (callbackYes != null)
            {
                callbackYes(fullFilename);
            }
        }

        public void No()
        {
            if (callbackNo != null)
            {
                callbackNo();
            }
        }

        public delegate void Del1();
        public delegate void Del2(string s);

        public Del2 callbackYes;
        public Del1 callbackNo;
    }
}